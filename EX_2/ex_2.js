function tienDien() {
  var hoTen = document.getElementById("ho-ten").value;
  var soKW = document.getElementById("so-kw").value * 1;

  // tính tiền điện
  var result = 0;
  if (soKW <= 50) {
    result = soKW * 500;
  } else if (soKW <= 100) {
    result = 50 * 500 + (soKW - 50) * 650;
  } else if (soKW <= 200) {
    result = 50 * 500 + 50 * 650 + (soKW - 50 - 50) * 850;
  } else if (soKW <= 350) {
    result = 50 * 500 + 50 * 650 + 100 * 850 + (soKW - 50 - 50 - 100) * 1100;
  } else {
    result =
      50 * 500 +
      50 * 650 +
      100 * 850 +
      150 * 1100 +
      (soKW - 50 - 50 - 100 - 150) * 1300;
  }

  // in kết quả
  document.getElementById("result").innerHTML = `<h3 class="py-4">
  Tên khách hàng: ${hoTen} </br>
  Số tiền điện khách hàng cần thanh toán là: ${result.toLocaleString()} VND
  </h3>`;
}
